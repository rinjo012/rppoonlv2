﻿using ConsoleApp5.Class;
using System;
using System.Collections.Generic;

namespace ConsoleApp5
{

    class Program
    {
        static void PrintAllDice(IList<int> dice)
        {
            foreach (var Die in dice)
                System.Console.WriteLine(Die);
        }

        static void Main(string[] args)
        {
            //1. zad

            //DiceRoller diceRoller = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //    diceRoller.InsertDie(new Die(6));

            //diceRoller.RollAllDice();

            //PrintAllDice(diceRoller.GetRollingResults());

            Random rng = new Random();
            DiceRoller diceRoller = new DiceRoller();

            for (int i = 0; i < 20; i++)
                diceRoller.InsertDie(new Die(6, rng));

            diceRoller.RollAllDice();

            PrintAllDice(diceRoller.GetRollingResults());
        }
    }

}